import express from 'express';
import morgan from 'morgan';
import dotenv from 'dotenv';
import homeRouter from './routes/home';

// Load environment variables from .env file
dotenv.config();

const app = express();

// Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: true }));

// Routes
app.use('/', homeRouter);

// Start the server
const port = process.env.PORT ?? 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
